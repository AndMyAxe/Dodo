# Coding Style Guidelines

We are using the standardJS style. See https://standardjs.com

In addition to standardJS we have some other guidelines for the project listed
below.

- Line widths should be limited to 80 characters when possible. Long lines that
    get split should have the extra lines indented by one tab-width compared to
    the first part of the line.
- Make lots of comments. The goal is to make the code understandable enough with
    comments that someone who is unfamiliar with the code can understand what is
    going on by just reading the code.
- Every file should have a block comment at the top of the file that describes
    what is in the file. If we are well organised and we have lots of files
    having a list of functions defined in a file may be helpful too.
- Every function should have a block comment above the function definition that
    describes the function and if possible lists places where it is used.
- Place lots of in-line comments
- In-line comments should generally be on their own line above what they are
    describing. Execptions are very short comments describing things like
    constants. E.g.: `var bob = 6.28 // 2*PI`
- If there are places in the code where we know there will be changes in the
    future mark them with `TODO` inside a block comment with a quick description
    of the changes. This is so that we can find the spots easily and so that we
    know planned changes when making edits. E.g.:
    `/*TODO make a function instead of using a 10.000 line look-up table */`
- For html tags, put each attribute on its own line when possible. E.g.

    ```
    <div
      style='border:solid'
    >
      HI!
    </div>
    ```
