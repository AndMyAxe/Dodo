# Dodo

# HUGE CHANGES IN THE WORKS!! ALMOST NOTHING IN THIS IS CURRENT!!

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

Dodo is a distributed social media platform where each user is the owner of
their content.

Each person runs Dodo on their device and it works as their node in the network
without needing an external server because each person is their own server.
The client and the server are the same thing in this case.

To see more in-depth documentation see the `/Documentation` folder.

## installation

In the future this should be easier. At the moment this doesn't do much.

1. Make sure you have nodejs and npm installed for your operating system. See
  instructions at https://nodejs.org/en/
2. Make sure you have git installed for your operating system. See instructions
  at https://git-scm.com/downloads
3. Clone the repo
  1. In a terminal navigate to where you want to install Dodo
  2. Type `git clone git@gitlab.com:AndMyAxe/Dodo.git`
  3. Go to the newly created folder (Type `cd Dodo`)
4. Install dependencies (In the Dodo folder type `npm install`)
5. Configure Dodo by renaming the the file
  `Config/Example-Local.json` to `Config/Local.json` and changing the server
  name to what you want to call your node. (There will be more configuration
  options later)
6. Start Dodo by typing `npm start`

After installation you can open a browser and go to localhost:3000, you can
pick either documentation (which is very very outdated, look at the
documentation in the repo instead) or go to the Interface. After clicking on
`Interface` it will ask you for a username and password. By default the
username is `Bob` and the password is `password`. At the moment all you can do
from this interface is make posts. To change the username and password for the
local server edit your Local.json file and change the `username` and `password`
subproperties in the `secrets`.

### installation options

Change options by editing `Local.json` in the `Config` folder. Read the
`README.md` in `Config` for more about this.

I am working on a better method for configuration. The plan is to make
a system where when you run Dodo for the first time you have a configuration
wizard that updates `Local.json`.

## Design principles

These are the ideals that the project is built on. There are plenty of places
where it falls short, but these are the goals. These are a work in progress.

- Human centered - everything has to be based on the people using it, not just
  on what interesting technology is available. That said interesting technology
  is my main interest while making it.
- Accessibility - While in the early stages there are not many things that can
  be done in terms of accessibility in the normal sense. But even so the code
  must be as accessible as possible. This means
  - well documented code
  - code that can run on cheap and accessible hardware
  - an installation process that requires as little technical knowledge as
    possible
  - update and maintenance tools that follow the same principles
  - The front- and back-end components must be as independent as possible to
    allow people to easily choose how they wish to interact with the
    software.
- Opt-in, not opt-out - everything that can be should be opt-in, not opt-out.
  So you say who you want to share anything with, and who you want to see
  things from.
- Ownership - You own anything you make. What this means in a federated system
  is a bit strange since you share things, this may need more said about it.

## Architecture

Dodo is a distributed network. This means there's no central server.

Its idea is based on federated networks like Mastodon/GNU-Social but in Dodo
there's no user aggregation. There's only one user per node, giving the user
absolute freedom on the content they post and share.

Dodo nodes consist on a simple database where the data is stored, an interface
to communicate with the owner of the node, another interface to interact with
other nodes in the network and a discovery service which allows nodes to join
and leave the network.

### Database

The database system stores all the content uploaded by the owner of the
application.

The database system also stores some networking information and metadata
necessary for the communication protocol.

There are multiple databases used:

- `Posts` - This database holds the posts from the owner
  - `PostID` - A unique id for the post
  - `UserName` - The handle used by the post author
  - `Public` - a flag showing if the post is public or not
  - `Conversation` - the id of the conversation for the post. If no
    conversation id is 0.
  - `PostText` - The main body of the post.
  - `PostType` - A string indicating the type of post.
  - `TimeStamp` - A timestamp for the post in milliseconds since the linux
    epoch.
- `Connections` - This database holds the information about friends and
  followers and who is allowed to see what.
  - `ServerName` - the name used by the remote server
  - `ServerURL` - the URL of the server
  - `ServerGroup` - The group that the server belongs to. This can be used
    for privacy things and building timelines and the like.
  - `ServerToken` - The current access token for the server.
  - `ServerTokenExpiration` - The time when the access token expires. Due to
    weirdness with the JWT expiration times this may be in seconds since the
    linux epoch, or it may be in milliseconds rounded to the nearest second.
  - `ServerType` - This is a placeholder for if we have introduction servers
    or some sort of identity servers or something like that in the future.
  - `ServerAccessMethod` - This tells Dodo how the server is contacted. Some
    will be on dedicated static IPs but hopefully not all of them. This will
    determine what is used to contact the server.
  - `Public` - This indicates if the server information is public. If it is
    than it can be shared freely across the network, otherwise it shouldn't
    be. This is so you can have private or friends only accounts that aren't
    publicly visible.
- `RemotePosts` - This database holds posts from other servers
  - `OriginServer` - The server that the post originated from
  - `PostID` - A unique id for the post
  - `UserName` - The handle used by the post author
  - `Public` - a flag showing if the post is public or not
  - `Conversation` - the id of the conversation for the post. If no
    conversation id is 0.
  - `PostText` - The main body of the post.
  - `PostType` - A string indicating the type of post.
  - `TimeStamp` - A timestamp for the post in milliseconds since the linux
    epoch.

#### Data ownership and privacy

Ownership and privacy are achieved by private-public keypairs. Shared contents
are signed by users to ensure authenticity and ownership.

For privacy, the contents are encrypted using a private key and public keys are
only shared with other nodes allowed to see the contents.

### Client

Dodo is accessed from a REST API listening in localhost:3000 by default.

### Node discovery and communication

> TODO Use the documentation in the /Documentation/ folder to expand this
