const mesh = require('./mesh.js')
//const serverRequests = require('./requests/server-requests.js')
let history = []
let connections = []
const WebSocketServer = require('ws').Server
const wss = new WebSocketServer({noServer: true})

const start = function (config, handlers, requests, mesh) {
  console.log('starting chat server')
  // Ensure that all of the config values needed exist, using defaults if they
  // are missing.
  config.server.name = config.server.name || 'Awe Inspiring Test Server'
  // Set the onconnection function
  wss.on('connection', handleConnection)

  function handleConnection(client) {
    console.log("new connection")
    connections.push(client)
    //Get the list of connected people here
    var connectedPeople = Object.keys(mesh.connections).map(function(id){return mesh.connections[id].name})
    var message = JSON.stringify({
      type: 'connections',
      peers: connectedPeople
    })
    connections.forEach(function(socket) {
      socket.send(message)
    })
    // Message handler
    client.on('message', function incoming(event) {
      var connectedPeople = Object.keys(mesh.connections).map(function(id){return mesh.connections[id].name})
      //console.log(connectedPeople)
      var message = JSON.stringify({
        type: 'connections',
        peers: connectedPeople
      })
      connections.forEach(function(socket) {
        socket.send(message)
      })
      // Determine which connection the message came from
      try {
        var eventData = JSON.parse(event)
        if (eventData.type === 'chat') {
          var newMessage = {
            type: 'chat',
            text: eventData.message,
            name: config.server.name,
            time: String(eventData.time)
          }
          connections.forEach(function(socket) {
            socket.send(JSON.stringify(newMessage))
          })
          //socket.send(JSON.stringify(newMessage))
          //if (remote) {
          if (true) {
            var m = {'message': newMessage.text, 'from': newMessage.name, 'time': String(newMessage.time)}
            requests.sendMessage(m)
          }
          handlers.updateHistory(newMessage)
        } else if (eventData.type === 'updateHistory') {
          var message = JSON.stringify({type: 'updateHistory', history: history})
          connections.forEach(function(socket) {
            socket.send(message)
          })
        }
      } catch (e) {
        console.log("WebSocket error, probably closed connection: ", e)
      }
    })
  }
}

const handlers = {}
handlers.updateHistory = function (newMessage) {
  // Add message to chat history
  history.push(newMessage)
}

//const addMessage = function (message) {
handlers.addMessage = function (message) {
  const decoded = JSON.parse(message.toString())
  const newMessage = {
    type: 'chat',
    text: decoded.message,
    name: decoded.from,
    time: decoded.time
  }
  connections.forEach(function(socket) {
    socket.send(JSON.stringify(newMessage))
  })
}


module.exports = {}
module.exports.wss = wss
module.exports.start = start
//module.exports.requests = requests
module.exports.handlers = handlers
