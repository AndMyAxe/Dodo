/*
  This is a wrapper that is here so that you can always just require this file
  and it will work regardless of the database you are actually using.

  This means that every database plugin needs to have a consistent set of
  interface functions.
*/

// Get the global configuration
//var config = require('../LoadConfig.js')

// Load the correct database adaptor
const db = require(config.plugins.database.settings['adapter-path'])

// Export the database adatpor
module.exports = {}
module.exports.handlers = db.handlers
module.exports.requests = db.requests
