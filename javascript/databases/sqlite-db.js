// This adds our sqlite database
var sqlite3 = require('sqlite3')

// create the database object
var db = new sqlite3.Database('./Post.db')

// make sure that the tabel we want to use actually existts
db.run('CREATE TABLE IF NOT EXISTS Posts (PostID INTEGER PRIMARY KEY AUTOINCREMENT, UserName TEXT, Public BOOLEAN, Conversation INTEGER, PostText TEXT, PostType TEXT, TimeStamp INTEGER)')

// Make sure that the connections database exists
db.run('CREATE TABLE IF NOT EXISTS Connections (ServerName TEXT, ServerURL TEXT, ServerGroup TEXT, ServerToken TEXT, ServerTokenExpiration INTEGER, ServerType TEXT, ServerAccessMethod TEXT, Public BOOLEAN, ServerID TEXT)')

// Make sure that the RemotePosts database exists
db.run('CREATE TABLE IF NOT EXISTS RemotePosts (OriginServer TEXT, PostID INTEGER, UserName TEXT, Public BOOLEAN, Conversation INTEGER, PostText TEXT, PostType TEXT, TimeStamp INTEGER)')

// Perpared statements to help with database sanitization

// queries to get all posts
var GetAllPosts = db.prepare('SELECT * FROM Posts ORDER BY PostID DESC')
var GetPersonsPosts = db.prepare('SELECT * FROM Posts WHERE UserName=$name ORDER BY PostID DESC')
var GetConversationPosts = db.prepare('SELECT * FROM Posts WHERE Conversation=$thread')

// queries to get public posts
var GetAllPublicPosts = db.prepare('SELECT * FROM Posts WHERE Public="true" ORDER BY PostID DESC')
var GetPersonsPublicPosts = db.prepare('SELECT * FROM Posts WHERE UserName=$name AND Public="true" ORDER BY PostID DESC')
var GetConversationPublicPosts = db.prepare('SELECT * FROM Posts WHERE Conversation=$thread AND Public="true"')

// query to make a post
var MakePost = db.prepare('INSERT INTO Posts (UserName, PostText, Public, Conversation, PostType, TimeStamp) VALUES ($name, $text, $public, $conversation, $postType, $timeStamp)')

// This is what you get if you aren't authenticated (not the owner)
var GetPublicServerList = db.prepare('SELECT ServerName, ServerURL, ServerType, ServerAccessMethod FROM Connections WHERE Public="true"')
// This is what you get when you are authenticated (you are the owner)
var GetServerList = db.prepare('SELECT ServerName, ServerURL, ServerGroup, ServerTokenExpiration, ServerType, ServerAccessMethod, Public FROM Connections')

// Store initial information for a server
var StoreServerInfo = db.prepare('INSERT INTO Connections (ServerName, ServerURL, ServerGroup, ServerType, ServerAccessMethod, Public, ServerID) VALUES ($serverName, $serverURL, $serverGroup, $serverType, $serverAccessMethod, $public, $serverID)')

// Update the access token for a server
var UpdateServerToken = db.prepare('UPDATE Connections SET ServerToken=$token, ServerTokenExpiration=$tokenExpiration WHERE ServerName=$serverName AND ServerURL=$serverURL')

// Update the group a server belongs to
var UpdateServerGroup = db.prepare('UPDATE Connections SET ServerGroup=$serverGroup WHERE ServerName=$serverName AND ServerURL=$serverURL')

// Save posts made on another server
var SaveRemotePost = db.prepare('INSERT INTO RemotePosts (OriginServer, UserName, PostText, Public, Conversation, PostType, TimeStamp) VALUES ($serverName, $name, $text, $public, $conversation, $postType, $timeStamp)')

module.exports = db
module.exports.GetAllPosts = GetAllPosts
module.exports.GetAllPublicPosts = GetAllPublicPosts
module.exports.GetPersonsPosts = GetPersonsPosts
module.exports.GetPersonsPublicPosts = GetPersonsPublicPosts
module.exports.GetConversationPosts = GetConversationPosts
module.exports.GetConversationPublicPosts = GetConversationPublicPosts
module.exports.MakePost = MakePost
module.exports.GetPublicServerList = GetPublicServerList
module.exports.GetServerList = GetServerList
module.exports.StoreServerInfo = StoreServerInfo
module.exports.UpdateServerToken = UpdateServerToken
module.exports.UpdateServerGroup = UpdateServerGroup
module.exports.SaveRemotePost = SaveRemotePost
