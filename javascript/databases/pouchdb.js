const path = require('path')
const fs = require('fs')

//var settings = require(__dirname + '/LoadConfig.js')

var PouchDB = require('pouchdb')
//var peopleDB = new PouchDB('people')
//var configDB = new PouchDB('ConfigDB')

// This object holds all of the available database objects
var databaseObj = {}
// This object holds the sync objects for any databases that are syncing
// You use these for cancelling syncs and any other modification to it
var syncObj = {}

/*
  This gets the settings for the local machine from the database.
  This should be run once when the application starts and then doesn't have to
  run until it is restarted.
*/
function getLocalSettings() {
  initialiseDatabase('configDB')
  // Check each setting to see if it is in the configDB and update the setting
  // if it is
  databaseObj['configDB'].get('_local/settings').then(function(doc) {
    Object.keys(doc).forEach(function(key) {
      if (key !== '_id' && key !== '_rev') {
        settings[key] = doc[key]
      }
    })
  }).catch(function(err) {
    if (err.name === 'not_found') {
      var newDoc = JSON.parse(JSON.stringify(settings))
      newDoc['_id'] = '_local/settings'
      databaseObj['configDB'].put(newDoc).catch(function(err) {
        return err
      })
    } else {
      return err
    }
  })

  var settingsList = Object.keys(settings)
  settingsList.forEach(function(settingName) {
    databaseObj['configDB'].get(`_local/${settingName}`).then(function(doc) {
      // Use the value from the DB
      settings[settingName] = doc.value
    }).catch(function(err) {
      if (err.name !== 'not_found') {
        // If error is that the the database doesn't have a value don't do
        // anything, otherwise print the error.
        return err
      }
    })
  })
}

/*
  This updates the local settings. This is used to save anything specific to
  the current machine to the database.
*/
function setLocalSetting(key, value) {
  // Make sure database is initialised
  initialiseDatabase('configDB')
  databaseObj['configDB'].get('_local/settings').then(function(doc) {
    //Update setting
    doc[key] = value
    configDB.put(doc).catch(function(err) {
      return err
    })
  }).catch(function(err) {
    if (err.name === 'not_found') {
      //Add new setting
      doc = {_id: '_local/settings'}
      doc[key] = value
      databaseObj['configDB'].put(doc).catch (function(err) {
        return err
      })
    } else {
      //print error
      return err
    }
  })
}

/*
  This initialises a database, either opening it if it exists or creating it if
  it doesn't, and returns the database object.
  If the database is already open it just returns the database object.
  you don't need to save the returned object because it is stored in the databaseObj object using the database name as key.
*/
function initialiseDatabase(name) {
  if (typeof name === 'string') {
    if (!name.startsWith(settings.dbRootFolder)) {
      fullName = path.resolve(settings.dbRootFolder, name)
    } else {
      fullName = path.resolve(name)
    }
    var pieces = fullName.split(path.sep).filter(function(piece) {return piece.trim().length > 0})
    if (pieces.length > 1) {
      for (i = 0; i < pieces.length - 1; i++) {
        var joinedPath = pieces.slice(0,i+1).join(path.sep)
        if (!path.isAbsolute(joinedPath)) {
          joinedPath = path.sep + joinedPath
        }
        if (!fs.existsSync(joinedPath)) {
          fs.mkdirSync(joinedPath)
        }
      }
    }
  } else {
    return null
  }
  if (typeof name === 'string' && typeof databaseObj[name] === 'undefined') {
    var useMe = path.relative(process.cwd(), fullName)
    databaseObj[name] = new PouchDB(useMe)
  }
  return databaseObj[name]
}

/*
  This retrieves a document from an existing database and returns it
*/
function fetchDoc(dbName, docName) {
  if (typeof databaseObj[dbName] !== 'undefined') {
    return databaseObj[dbName].get(docName).then(function(doc) {
      return doc
    }).catch(function(err) {
      return err
    })
  } else {
    return {ok: false, error: 'Database Not Found'}
  }
}

/*
  This retrieves a specific key value from within a given doc and returns it
*/
function fetchValue(dbName, docName, key) {
  if (typeof databaseObj[dbName] !== 'undefined') {
    return databaseObj[dbName].get(docName).then(function(doc) {
      return doc[key]
    }).catch(function(err) {
      if (err.name === 'not_found') {
        return undefined
      } else {
        return err
      }
    })
  } else {
    return {ok: false, error: 'Database Not Found'}
  }
}

/*
  This writes to an existing database, returning an error if the database
  doesn't exist.
  This will overwrite an existing document if any exist with the same name.
*/
function writeDoc(dbName, doc) {
  if (typeof databaseObj[dbName] !== 'undefined') {
    return databaseObj[dbName].get(doc._id).then(function(existing) {
      // Do something when it already exists
      doc._rev = existing._rev
      return databaseObj[dbName].put(doc).then(function(response) {
        return response
      }).catch(function(err) {
        return err
      })
    }).catch(function(err) {
      if (err.name === 'not_found') {
        // write the new doc
        return databaseObj[dbName].put(doc).then(function(response) {
          return response
        }).catch(function(err) {
          return err
        })
      } else {
        return err
      }
    })
  } else {
    return {ok: false, error: 'Database Not Found'}
  }
}

/*
  This updates an existing doc with some values
  This will not overwrite the whole document, it only changes values that are
  included in the update object.
  If create is set to true and the document doesn't exist than a new one is
  created using the fields in the update object.
  Otherwise if the document doesn't exist an error is returned.
*/
function updateDoc(dbName, docName, update, create) {
  if (typeof databaseObj[dbName] !== 'undefined') {
    return databaseObj[dbName].get(docName).then(function(doc) {
      Object.keys(update).forEach(function(key) {
        doc[key] = update[key]
      })
      return databaseObj[dbName].put(doc).then(function(response) {
        return response
      }).catch(function(err) {
        return err
      })
    }).catch(function(err) {
      if (err.name === 'not_found') {
        if (create === true) {
          return databaseObj[dbName].put(update).then(function(response) {
            return response
          }).catch(function(err) {
            return err
          })
        } else {
          return err
        }
      } else {
        return err
      }
    })
  } else {
    return {ok: false, error: 'Database Not Found'}
  }
}

/*
  This sets a remote database to sync with a local database

  For syncing databases we will keep a record of when the last sync was, if
  any, so we can start the sync process at that point.
*/
function setSync(dbName, remote) {
  if (syncObj[dbName] !== 'undefined') {
    // Already syncing
    return syncObj[dbName]
  } else {
    // Resync
    if (typeof databaseObj[dbName] !== 'undefined' && typeof remote === 'string') {
      syncObj[dbName] = PouchDB.sync(dbName, remote, {
        live: true
      })
      syncObj[dbName].on('paused', function(err) {
        // Stop it and remove the object from the syncObj
        this.cancel()
        syncObj[dbName] = undefined
      }).on('denied', function (err) {
        // Stop it and remove the object from the syncObj
        this.cancel()
        syncObj[dbName] = undefined
      }).on('error', function(err) {
        // Stop it and remove the object from the syncObj
        this.cancel()
        syncObj[dbName] = undefined
      }).on('complete', function(info) {
        // Stop it and remove the object from the syncObj
        this.cancel()
        syncObj[dbName] = undefined
      })
      return syncObj[dbName]
    }
  }
}

function stopSync(dbName, remote) {
  if (syncObj[dbName] !== 'undefined') {
    syncObj[dbName].cancel()
    syncObj[dbName] = undefined
  }
}

const pluginHandlers = {}
pluginHandlers.updateHistory = function () {

}

pluginHandlers.fetchHistory = function () {

}

pluginHandlers.storePeerInfo = function () {
  
}

function start (config, handlers, requests, mesh) {

}

module.exports = {}
module.exports.start = start
//module.exports.requests = requests
module.exports.handlers = pluginHandlers
/*
module.exports.getLocalSettings = getLocalSettings
module.exports.setLocalSetting = setLocalSetting
module.exports.initialiseDatabase = initialiseDatabase
module.exports.fetchDoc = fetchDoc
module.exports.fetchValue = fetchValue
module.exports.writeDoc = writeDoc
module.exports.updateDoc = updateDoc
module.exports.setSync = setSync
module.exports.stopSync = stopSync
*/
