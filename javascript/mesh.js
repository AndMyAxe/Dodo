/*
  This file is to ensure that every other plugin gets the same mesh object.

  It is used in many other places.

  Information baout the mesh object is in

  Documentation/Development/The Mesh Object.md

  TODO write up descriptions of currently used message types

  TODO Make a way for us to save information about nodes so it is persistent
  between runs.
  This also requires a way to add to the mesh object outside of
  mesh-discovery.js
*/

var mesh = {connections:{}}

module.exports = mesh
