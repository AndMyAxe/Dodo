/*
  This lets us have a global set of data handlers.

  Server requests are requests made to other nodes initiated by the local node.

  requests that are exported in plugins are added to this requests object so
  that they will be available to all plugins.
*/

// Initialise the global handlers object
let requests = {}

function addRequests (newRequests) {
  if (typeof newRequests === 'object') {
    Object.keys(newRequests).forEach(function(requestName) {
      if (typeof newRequests[requestName] === 'function') {
        requests[requestName] = newRequests[requestName]
      }
    })
  }
}

module.exports = requests
module.exports.addRequests = addRequests
