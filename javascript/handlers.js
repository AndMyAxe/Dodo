/*
  This lets us have a global set of data handlers.

  Data handlers are for doing something with incoming messages. The messages
  can be from an external source or from a different plugin on the same node.

  handlers that are exported in plugins are added to this handlers object so
  that they will be available to all plugins.
*/

// Initialise the global handlers object
var handlers = {}

function addHandlers (newHandlers) {
  if (typeof newHandlers === 'object') {
    Object.keys(newHandlers).forEach(function(handlerName) {
      if (typeof newHandlers[handlerName] === 'function') {
        handlers[handlerName] = newHandlers[handlerName]
      }
    })
  }
}

module.exports = handlers
module.exports.addHandlers = addHandlers
