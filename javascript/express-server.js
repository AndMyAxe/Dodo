/*
  This is the express server used as the local interface for Dodo0
*/

// Initialise plugins object
let plugins = {}

/*
  This start function is setup so that it will only complete if the plugins
  have been loaded because we need to have at least the server requests loaded
  before this can function correctly.
*/
const start = function (config) {
  console.log('starting express server')
  // Get loaded plugins
  plugins = require('../LoadPlugins.js')
  if (Object.keys(plugins).length === 0) {
    // If we don't have plugins loaded yet don't start the server
    setTimeout(start, 100)
  } else {
    setup(config)
  }
}

/*
  This is called by the start function. It is split because we need to ensure
  that the plugins have loaded before starting this.
*/
const setup = function (config, handlers, requests, mesh) {
  // This is used to build the paths used by express
  const path = require('path')
  // This is the webserver framework thing
  const express = require('express')
  // This is used to parse the json body of POSTs made to the server
  const bodyParser = require('body-parser')

  // get the stuff to make our tokens
  const jwt = require('jsonwebtoken')
  // This is the part that has the server control functions
  //const serverRequests = require('./requests.js')
  // Get the database we are using
  var database = plugins.database

  // initialise the express application
  const app = express()

  /*
    Set constants.
    TODO
    This needs to be changed so they are loaded from a settings file or
    something similar.
  */
  const SUPER_DUPER_SECRET = 'AndJavertSaysHi'

  const serverURL = 'someURL'

  /*
    TODO
    This is only for testing, this needs to be loaded from some secure place
    instead of just left sittign here in plain text
  */

  const sloppyPassword = config.server.secrets.password
  const sloppyUser = config.server.secrets.username

  // Tell the express application to use Set
  //app.engine('html', set.__express)
  //app.set('view engine', 'html')

  // Tell the express application what the server secret is
  app.set('superSecret', SUPER_DUPER_SECRET)

  /*
    TODO
    organize the files better
    make a default simple GUI so we don't have to just use the tiddlywiki one I
    made.
  */
  /*
    Tell the application which folder to serve files from
    The first argument is where it is served as ('/' corresponds to
    'localhost:3000/' using the default settings. The second argument is the root
    folder holding the pages to serve.)
  */
  app.use('/', express.static(path.join(__dirname, '../Pages')))

  /*
    TODO
    check to make sure that the headers are set correctly. I think they are but
    check anyway.
  */
  // Tell the express application to use body-parser
  app.use(bodyParser.urlencoded({extended: false}))
  app.use(bodyParser.json())
  app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
  })

  // Tell the application which port to listen on
  config.plugins['express-server'].settings.port = config.plugins['express-server'].settings.port || 8080
  const server = app.listen(config.plugins['express-server'].settings.port, function () { console.log(`Listening on port ${config.plugins['express-server'].settings.port}`)
  })
  /*
    This is a mess and needs to be cleaned up
  */
  let wssThings = {}
  if (config.plugins['chat'].active) {
    wssThings['chat'] = require('./chat-server.js').wss
  }

  if (config.plugins['chat'].active) {
    // This lets the websocket interface use the same port as the express server
    server.on('upgrade', function(request, socket, head) {
      if (config.plugins['chat'].active) {
        if (request.url === '/socketchat') {
          wssThings['chat'].handleUpgrade(request, socket, head, function(ws) {
            wssThings['chat'].emit('connection', ws, request)
          })
        }
      }
    })
  }

  /*
      The simple authentication path. It checks to make sure that the name and
      password match what is expected and returns a token.

      This should only be used by the owner. We will hopefully have this optional
      and have it so that the owner can use a separate application locally and
      the express server is only used by other people fetching posts from this
      node.

      TODO
      I had some weird behaviour from this. expiresIn should be in seconds but
      the tokens weren't working when I gave it 600 seconds (10 minutes), but
      that may have been from some other error.
      Also I know that the value in res.json is wrong. I am also not sure that the value in res.json is ever used.
  */
  app.post('/api/authenticate', function (req, res) {
    console.log(req.body)
    console.log(sloppyUser)
    console.log(sloppyPassword)
    if (sloppyPassword === req.body.password && sloppyUser === req.body.name) {
      // make a token
      // Expires in 10 hours?
      const token = jwt.sign({data: req.body.name}, app.get('superSecret'), {expiresIn: 60 * 60 * 10})
      res.json({
        success: true,
        token: token,
        expires: Date.now() + 10 * 60 * 60
      })
    } else {
      res.json({
        success: false,
        token: null
      })
    }
  })

  /*
    This is the api endpoint you point to when you are adding the node as a
    friend. It returns the information needed to access the node in the future.
    The person receiving the information needs to put this into their Connections
    database table.

    This is always in JSON because doing this as a human readable thing isn't
    useful.

    If it is useful adding that isn't hard. But the default should be JSON.

    TODO
    Make the other side of this!

    TODO
    Figure out where this information should be defined locally

    TODO
    Figure out how we are going to have people make connections to private nodes.
    It may require some other channel, or a one-time use password or something.
  */
  app.get('/api/info', function (req, res) {
    if (config.server.public) {
      res.json({
        'ServerName': config.server.name,
        'ServerURL': serverURL,
        'ServerType': config.server.type,
        'ServerAccessMethod': config.server.accessmethod,
        'Public': config.server.public
      })
    } else {
      res.status(404)
    }
  })

  app.post('/api/info', function (req, res) {
    if (config.server.public) {
      res.json({
        'ServerName': config.server.name,
        'ServerURL': serverURL,
        'ServerType': config.server.type,
        'ServerAccessMethod': config.server.accessmethod,
        'Public': config.server.public
      })
    } else {
      res.status(404)
    }
  })

  /*
    This defines the api endpoint that returns information for all public servers
    that are known to the current server
  */
  app.get('/api/public/servers/All', function (req, res) {
    database.GetAllServers.all({}, function (err, rows) {
      if (err) {
        res.send(err)
      } else {
        res.render('./server-list-template.html', {'servers': rows})
      }
    })
  })

  /*
    This defines the api endpoint that returns information for all public servers
    that are known to the current server in json format.
  */
  app.get('/api/public/servers/All/json', function (req, res) {
    database.GetAllServers.all({}, function (err, rows) {
      if (err) {
        res.send(err)
      } else {
        res.send(JSON.stringify(rows))
      }
    })
  })

  /*
    This defines the api endpoint that returns all public posts in a readable
    way

    This is very simple, it just runs the database query that returns all posts
    by everyone and returns rendered html using the post-template.html template.

    TODO
    only have this return posts that are tagged as public
  */
  app.get('/api/public/people/All', function (req, res) {
    database.GetAllPublicPosts.all({'$name': req.params.name}, function (err, rows) {
      if (err) {
        res.send(err)
      } else {
        res.render('./post-template.html', {'posts': rows})
      }
    })
  })

  /*
    This defines the api endpoint that returns all posts in JSON format

    This is the same as /api/public/people/All but the returned values are in
    json from (computer readable) instead of rendered html.
  */
  app.get('/api/public/people/All/json', function (req, res) {
    database.GetAllPublicPosts.all({'$name': req.params.name}, function (err, rows) {
      if (err) {
        res.send(err)
      } else {
        res.send(JSON.stringify(rows))
      }
    })
  })

  /*
    This defines the api endpoint that returns all posts from a person in a
    readable way

    This is the same as /api/public/people/All but it only returns posts by
    :name
  */
  app.get('/api/public/people/:name', function (req, res) {
    database.GetPersonsPublicPosts.all({'$name': req.params.name}, function (err, rows) {
      if (err) {
        res.send(err)
      } else {
        res.render('./post-template.html', {'posts': rows})
      }
    })
  })

  /*
    This defines the api endpoint that returns all posts from a person in JSON

    This is the same as /api/public/people/All/json but it only returns posts
    by :name
  */
  app.get('/api/public/people/:name/json', function (req, res) {
    database.GetPersonsPublicPosts.all({'$name': req.params.name}, function (err, rows) {
      if (err) {
        res.send(err)
      } else {
        res.send(JSON.stringify(rows))
      }
    })
  })

  /*
    This is the first attempt at making controls for inter-server communication
  */
  app.post('/api/local/fetch', function (req, res) {
    //serverRequests.FetchAllPublicPosts(req.params.url)
    requests.FetchAllPublicPosts(req.params.url)
  })

  // Set up the routes for authentication
  const authenticationRoutes = express.Router()

  /*
    Everything below here has to be authenticated using the function defined
    below.

    This makes sure that there is a token, it checks to make sure it is valid
    using the server secret, then makes sure that the token hasn't expired and
    that the name in the token matches the name of the post.

    TODO
    We need to add different types of authentication. One to allow posts and
    one te allow retrieving posts.

    For the posting authentication we need to make sure that the name the post
    is being made under matches the name on the token (no masquerading as
    someone else).

    For retrieving posts we just need to make sure that the name and password
    match a valid pair. We need to figure out how we are storing names and
    passwords.

    One part of the difficulty here is that each person who access this may
    have a different set of things that they are authorised to see.
  */
  authenticationRoutes.use(function (req, res, next) {
    // Get the token
    function getCookie(cname) {
      const name = cname + "=";
      const cookie = req.headers['cookie']
      if (cookie) {
        const ca = cookie.split(';');
        for(let i = 0; i <ca.length; i++) {
          let c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
      }
      return false;
    }
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || getCookie('token')

    // Verify the token
    // Check to make sure it hasn't expired and that the name it is signed with
    // is the same name as the incoming post
    // Also the server secret gets checked. We should look at how to do that
    // correctly.
    if (token) {
      jwt.verify(token, app.get('superSecret'), function (err, decoded) {
        if (err) {
          console.log('failed')
          return res.json({success: 'false', message: 'Token is not valid', expires: 0})
        } else {
          console.log(decoded)
          if ((decoded.exp > Date.now() / 1000)) { // && (decoded.data === req.body.name)) {
            req.decoded = decoded
            next()
          } else {
            console.log('Expired token or wrong name')
            return res.status(403).send({success: false, message: 'Expired Token', expires: '0'})
          }
        }
      })
    } else {
      console.log('No Token')
      return res.status(403).send({success: false, message: 'No Token', expires: '0'})
    }
  })

  // Anything below this that has a url under /api/people requires authentication
  app.use('/api/people', authenticationRoutes)

  // The server list
  app.get('/servers', function (req, res) {
    database.GetServerList.all(function (err, rows) {
      res.render('./server-list-template.html', rows)
    })
  })

  // The chat UI, it requires authentication
  app.use('/chat', authenticationRoutes)
  app.get('/chat', function (req, res) {
    res.render('./chat-view.html')
  })

  // The main UI also uses the same authentication route
  /*
    TODO
    get a better name for this
    make a separate authentication thing for it to make sure that this is only
    available to the owner
  */
  app.use('/main', authenticationRoutes)

  app.post('/main', function (req, res) {
    database.GetAllPosts.all({'$name': req.params.name}, function (err, rows) {
      if (err) {
        res.send(err)
      } else {
        res.render('./default-view.html', {'posts': rows})
      }
    })
  })

  /*
    This is currently how commands are sent from the browser to the node process
  */
  app.post('/main/command', function (req, res) {
    // this is where commands get taken care of
    var body = JSON.parse(req.body.body)
    body.options.from = config.server.name
    //serverRequests[body.command](body.options)
    requests[body.command](body.options)
  })

  /*
    This is the part that lets you post

    There is an additional check here to ensure that the authentication token
    matches the name that the post is made as.
    If they don't match this returns a 403 error.
  */
  app.post('/api/people/:name/post', function (req, res) {
    if (req.decoded.data === req.params.name) {
      database.MakePost.run({'$name': req.params.name, '$text': req.body.text, '$public': req.body.public, '$conversation': req.body.conversation, '$postType': req.body.postType, '$timeStamp': Date.now()})
      res.status(200).send({success: true, message: 'Posted'})
    } else {
      console.log(req.decoded)
      console.log(req.params.name)
      console.log('Bad Token maybe?')
      return res.status(403).send({success: false, message: "Token and name don't match!", expires: '0'})
    }
  })

  /*
    This defines the api endpoint that returns all posts in a readable way

    This is like /api/public/people/All but it shouldn't just be limited to
    public posts.

    TODO
    make this require authentication
  */
  app.post('/api/people/All', function (req, res) {
    database.GetAllPosts.all({'$name': req.params.name}, function (err, rows) {
      if (err) {
        res.send(err)
      } else {
        res.render('./post-template.html', {'posts': rows})
      }
    })
  })

  /*
    This defines the api endpoint that returns all posts in JSON format

    TODO
    make this require authentication
  */
  app.post('/api/people/All/json', function (req, res) {
    database.GetAllPosts.all({'$name': req.params.name}, function (err, rows) {
      if (err) {
        res.send(err)
      } else {
        res.send(JSON.stringify(rows))
      }
    })
  })

  /*
    This defines the api endpoint that returns all posts from a person in a
    readable way

    TODO
    make this require authentication
  */
  app.post('/api/people/:name', function (req, res) {
    database.GetPersonsPosts.all({'$name': req.params.name}, function (err, rows) {
      if (err) {
        res.send(err)
      } else {
        res.render('./post-template.html', {'posts': rows})
      }
    })
  })

  /*
    This defines the api endpoint that returns all posts from a person in JSON

    TODO
    make this require authentication
  */
  app.post('/api/people/:name/json', function (req, res) {
    database.GetPersonsPosts.all({'$name': req.params.name}, function (err, rows) {
      if (err) {
        res.send(err)
      } else {
        res.send(JSON.stringify(rows))
      }
    })
  })
}

module.exports = {}
module.exports.start = start
