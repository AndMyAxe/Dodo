const network = require('@hyperswarm/network')
const crypto = require('crypto')
//const chat = require('./chat-server.js')
let connections = []
let pluginRequests = {}

function start(config, handlers, requests, mesh) {
  console.log('starting hyperswarm')
  // This makes it so that we use the bootstrap node set in the settings if any, otherwise use the defaults
  config.plugins.hyperswarm.settings = config.plugins.hyperswarm.settings || {}
  const options = {
    bootstrap: config.plugins.hyperswarm.settings.bootstrap || undefined
  }
  // Create the network object
  const net = network(options)
  // Set the topic from the settings
  const topic = crypto.createHash('sha256').update(config.server.topic).digest()

  // This is a placeholder for later when we have a better idea of what we need to check for incoming connections.
  function authenticateConnection(thisSocket, connectionDetails) {
    return true
  }
  // Make sure we can punch holes in the network
  net.discovery.holepunchable(function(error,yes) {
    if (error) {
      console.log(error)
    }
      console.log('hole punchable')
      joinOptions = {
        lookup: true,
        announce: true
      }
      // Join the topic we set
      net.join(topic,joinOptions)

      // On connection save the socket and set up the message handlers
      net.on('connection', (socket, details) => {
        console.log('new connection!', details)
        // Only allow connections that are authenticated
        // TODO implement this!!
        const authenticated = authenticateConnection(socket,details)
        if (authenticated) {
          connections.push(socket)
          socket.on('data', function(data) {
            console.log('socket data')
            try {
              // Try parsing the message
              let message = data
              if (typeof data !== 'object') {
                message = JSON.parse(data.toString())
              }
              /*
              if (messate.type) {
                handlers[message.type](message)
              }
              */
              //chat.addMessage(data)
              handlers.addMessage(message)
            } catch (e) {
              console.log('some problem with the message ',e)
            }
          })
          // Handle closed connections by removing them
          socket.on('close', function(data) {
            connections = connections.filter(function(connection) {
              return connection.readyState === 'open'
            })
          })
        }
      })
  })
}

pluginRequests.sendMessage = function (message) {
  connections.forEach(function(thisSocket) {
    if(thisSocket.readyState === 'open') {
      if (typeof message === 'object') {
        try {
          message = JSON.stringify(message)
        } catch (e) {
          console.log('Error trying to coerce message into a sendable format: ', e)
        }
      }
      //console.log(thisSocket.address())
      thisSocket.write(message)
    }
  })
}

process.once('SIGINT', function () {
  console.log('Shutting down ...')
  net.discovery.destroy()
  net.discovery.on('close', function () {
    process.exit()
  })
})

module.exports = {}
module.exports.start = start
//module.exports.sendMessage = sendMessage
module.exports.requests = pluginRequests
//module.exports.handlers = handlers
