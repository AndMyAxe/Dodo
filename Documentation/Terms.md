# Glossary and Definitions

There is a lot of stuff here so I am making a list of terms so that they are
understood and used consistently.

- `Node` an instance of Dodo running somewhere. This is used interchangeably
  with the person using the node when talking about actions.
  - `Local Node` the node that you are using.
  - `Remote Node` any node other than the one you are using.
- `Connection` an active connection between nodes. It is active in the sense
  that it can immediately be used by the nodes to communicate.
- `Peer` or `Peers` other nodes in the network that are accessible, but may not
  have an active connection open.
- `Server` used interchangeably with `Node`, but normally in the context of the
  actual machine running the code, not in the context of activities or action
  taken by the person.
- `DHT` distributed hash table. This is used to store and access the
  distributed listing of node addresses.
- `Server Requests` or `ServerRequests` are messages used by the front-end to
  communicate with the server.
- `Data Handlers` or `DataHandlers` are the functions that are used to do
  things with things received when another server sends a message.
- `NAT Traversal` network address translation traversal
