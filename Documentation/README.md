# Dodo documentation

For now we don't have any usable parts for non-developers, so this is just
developer documentation.

This is a work in progress.

## What is this?

We had a lot of trouble finding references and learning what the state of the
art for distributed networking actually is while working on this project. So we
decided to document our findings so that hopefully the process is easier for
others in the future.

## Parts of the project

The project has a few separate pieces that can be examined more or less
independently.

- DHT (distributed hash table) - This is the part that allows the network to
  find and list nodes without requiring a central server. This part is perhaps
  the least well understood part. This is the main part that allows p2p
  applications to work without the hassles of using a DNS.
- NAT transversal - Normally while using the internet this is handled
  transparently by networking hardware because one end point has a static ip
  address and port which is publicly accessible. This is not the case for p2p
  applications. Common techniques used are UDP hole punching, TCP hole
  punching, and, TCP relays. This is the part that lets two computers
  communicate directly with each other when one or both are behind routers.
- Other bits of glue - Connecting the DHT to a usable application, distributed
  applications, etc.
- Security - Encryption and signing via JWT (JSON web tokens), REST-like apis
  (REST-like because we aren't using a strict client-server model)
- Privacy - Privacy is going to be part of all of this. The most visible
  part is going to be on the application itself and how it handles
  inter-personal interaction.
