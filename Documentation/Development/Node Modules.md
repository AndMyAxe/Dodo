# Node Modules

### npm modules used by Dodo

- `utp-native` used to create utp sockets for utp hole punching.
- `sqlite3` a module that adds bindings for sqlite databases. Used as the basic
database for Dodo.
- `jsonwebtoken` a module used for signing and encryption for posts and
communication.
- `indie-set` the templating engine used by the front-end.
- `express` the server used to create the front-end.
- `body-parser` a module that lets you parse http POSTs.
- `discovery-swarm` the module for hooking into the bittorrent dht and using it
to create connections to other nodes.

### core node modules used

- `path` for file system path operations, used by the express server.
- `fs` used for other file system operations.
