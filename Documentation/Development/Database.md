# Database

Current Dodo uses a sqlite database. This can be changed easily enough.

The interface to the database is defined by the `database.js` file in the
`javascript` folder of the repo. It loads the appropriate database handler as
defined in the configuration.

Any type of database should work as long as it supports the interface functions
defined below. These would be defined in the new database types plugin.

Dodo uses 3 tables (or separate databases? which is better?):

- Local Posts
- Remote Posts
- Connections

## Local Posts

This database holds all the posts made on the local node.
Entries in this database represent posts made and they contain the post content
and the associated metadata.

### Required interface functions

Entries marked with * are not implemented in the sqlite database yet.

- `GetAllPosts` - Retrieve all posts from the database. Can be given a limit.
- `GetPersonsPosts` - Retrieve all posts by a specific person in the database.
  Can be given a limit.
- `GetConversationPosts` - Retrieve all posts in a specific conversation from
  the database.
- `GetAllPublicPosts` - Get all posts marked as public in the database.
- `GetPersonsPublicPosts` - Get all posts by a specific person that are marked
  as public in the database.
- `GetConversationPublicPosts` - Get all posts belonging to a specific
  conversation in the database that are marked pubilc.
- `MakePost` - Create a post in the database.
- * `DeletePost` - Remove a post from the database.

## Remote Posts

This database holds the posts from remote nodes that are saved by the local
node

- `SaveRemotePost` - Save a post from a remote post locally.

## Connections

This database holds all of the information about connections to other nodes
that the local node knows.

- `GetServerList` - Get a list of all servers known by the local node.
- `GetPublicServerList` - Get a list of all public servers known by the local
  node.
- `StoreServerInfo` - Store information about a server locally.
- `UpdateServerToken` - Update the token from the remote server.
- `UpdateServerGroup` - Change the group that a remote server belongs to.


# Database Schema

The specific database schema used isn't necessarily uniform for every database
type. They just all have to have the required information.

## Required information for posts

This applies to both local and remote posts. Different post types may have
other information, but this is the basic part.

- `Author` - The author of the post
- `Post Body` - The main post body
- `Post ID` - A unique id for the post
- `Public` - A flag saying if the post is public or not (There should be more
  options!)
- `Conversation` - The conversation, if any, that the post belongs to.
- `Post Type` - The type of the post.
- `Time Stamp` - A timestamp for the post.
- `Origin Node` - The node ID for the node that made the post.

## Required information for connections

Since Dodo is made for single person nodes, information about a connection to a
node is equivalent to information about the connection to the person who uses
the node. Because of this a lot more can be added here. This information is a
mix of information about the remote node (server name, server type, etc.) and
local information assigned to the node (server group, nickname, etc.).

- `Server Name` - The name for the node. This may be the same as the name used by
  the person on the node.
- `Server ID` - The 160 bit id used by the node on the dht.
- `Server URL` - If there is a server URL. This would only be for nodes that have
  publicly accessible URLs. They could work as bootstrap nodes to get onto the
  DHT.
- `Server Group` - This is the group that the server belongs to. Set by the
  local node not the server. A similar idea to circles on google plus.
- `Server Token` - The JWT access token for the node.
- `Server Token Expiration` - The expiration for the JWT access token.
- `Server Type` - The server type.
- `Server Access Method` - How to access the node. Currently only `dht` for
  servers accessed via the distributed hash table.
- `Public` - If the node is publicly listed or not.
- `Bio` - A bio about the person who uses the node.
- `About` - About the node, maybe redundant since we have Bio?
- `Avatar` - An avatar to go with the node.
- `Last Updated` - A timestamp for the last time this information was updated.
  This is to check if a remote node has updated its information and the local
  node has to refetch it.
