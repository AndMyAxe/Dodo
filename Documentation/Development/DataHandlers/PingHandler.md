# ping message handler

This is what handles when a ping is received.

It responds by sending a pong server request back to the node that sent the
ping. This is mainly used for testing or to keep a connection alive.

The ping can have a body that is written to the console when it is received.
By default it says "Ping from serverName", and the pong reply says "pong from
serverName".

## A ping message has:

Example message: `{'t': 'ping', 'body': ping from ${config.server.name}}`

* `t` - `ping`
* `body` - `ping from ${config.server.name}`
