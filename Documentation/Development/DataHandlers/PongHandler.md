# pong message handler

This handles pong messages. See `PingHandler.md`.

## A Pong message has

Example message: `{'t': 'pong', 'body': pong from ${config.server.name}}`

* `t` - `pong`
* `body` - `pong from ${config.server.name}`
