# text message handler

This message is used for testing. When it is received it just prints out the
text contained in the body section.

## A text message has:

Example message: `{'t': 'text', 'body': "some text to display"}`

- `t` - `text`
- `body` - text to print
