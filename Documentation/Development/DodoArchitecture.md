# Dodo Architecture

Dodo uses a plugin architecture, the base code for Dodo is a simple framework
for loading plugins that doesn't do anything. The plugins add all of the actual
functionality.

The required parts of Dodo are:

- A way to store posts (a database) with the standard interface methods.
  Currently this is done with an sqlite database.
- A way to find and communicate with remove nodes. Currently this is done using
  the `mesh-discovery` plugin which uses the bittorrent dht.
- A way to interface with your local node. Currently this uses a local
  expressjs server.

Different applications can be built on top of this structure.
