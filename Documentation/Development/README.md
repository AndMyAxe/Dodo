# Dodo Developer Documentation

This is a work in progress.

This is documentation written for developers who what to either develop for
Dodo or want to understand how Dodo works for auditing or using the same
technologies in other projects.

This is also here to serve as an explanation for any design decisions we make.

Each subject has its own markdown file, read those for details.

Overview:

Dodo is plugin based. The main script that you run just loads the configuration
and then loads a list of plugins given by the configuration. All the
functionality comes from the plugins.

At a very high level Dodo has 3 components: A database, a UI and a way to
connect nodes together. This is a convenient abstraction for thinking about how
it works as a whole.

A probably very incomplete and inaccurate plan for the future is in
`Documentation/Development/Plan.md`

Add to this list as new things are added.

- Configuration documentation is in
`Documentation/Development/Configuration.md`
- Documentation about the database are in
`Documentation/Development/Database.md`
- Documentation for how data moves between nodes is described in
`Documentation/Development/DataFlow.md`
- The way that Dodo handles messages received is described in
`Documentation/Development/DataHandlers.md`
- Documentation for the DHT is in `Documentation/Development/Distributed Hash
Tables.md`
- The architecture of Dodo is described in
`Documentation/Development/DodoArchitecture.md`
- Documentation for the express server used is in
`Documentation/Development/ExpressServer.md`
- Documentation for the JSON Web Tokens is in `Documentation/Development/JSON
Web Tokens.md`
- Documentation for NAT Transversal is in `Documentation/Development/Nat
Transversal.md`
- The plugins for Dodo are described in `Documentation/Development/Plugins.md`
- A description of the mesh object used to keep track of nodes and connections
is in `/Documentation/Development/The Mesh Object.md`
