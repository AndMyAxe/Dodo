Concepts:

- UDP hole punching
- TCP hole punching
- TCP relays (relays that can be used as a sort of last resort when nothing
  else works)
- STUN servers (external servers to help with UDP hole punching)

Resources to add things from:

https://en.wikipedia.org/wiki/UDP_hole_punching

Example implementation of udp hole punching in node (requires STUN server I
think)

https://github.com/MicroMinion/udp-hole-puncher-js

Another very simple example set up including a server and two clients

https://github.com/SamDecrock/node-udp-hole-punching
