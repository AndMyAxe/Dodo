# Configuration

The configuration is controlled by the LoadConfig module.

It is similar to setting configuration variables but it allows you to have a
much more complex structure.

This module is used to ensure that you have a simple recovery option if you
somehow give invalid configuration options and to easly allow you to change
configuration options without danger of losing your settings.

## How it works

You require the `LoadConfig.js` and it returns a json object with the
configuration settings in it.

Ex: `var config = require('./LoadConfig.js')`

There are two configuration JSON files:
- The default configuration file (Config/Config.json)
- The local configuration file (Config/Local.json)

The default configuration file has settings that should always work and unless
you are a developer changing the default behaviour you shouldn't ever need to
change it.
The Local.json configuration file modifies or extends what is loaded from the
default configuration so you can have a different configuration on the local
computer.

If the Local.json file doesn't exist or for whatever reason can't be loaded
than the default configuration is loaded instead. If the default configuration
isn't valid than an empty object `{}` is returned for the configuration.

By requiring `LocalConfig.js` you can have a global configuration object for
project settings.

The json parsing is very strict and malformed json will cause problems. Be
careful about extra commas!

## Example

Example Config.json

```
{
  "SomeProperty": "value1",
  "SomeOther": "value2",
  "Thing": {
    "PropertyAgain": "thingy"
  }
}
```

Example Local.json

```
{
  "SomeOther": "value3",
  "Thing": {
    "LocalProperty": "otherThingy"
  }
}
```

Resulting config object:

```
{
  "SomeProperty": "value1",
  "SomeOther": "value3",
  "Thing": {
    "LocalProperty": "otherThingy",
    "PropertyAgain": "thingy"
  }
}
```
