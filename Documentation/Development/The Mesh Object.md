# The Mesh object

The mesh object contains all the currently known nodes in the network. It is
used to send information to those nodes and to handle connections to them.

The mesh object is originally defined in `javascript/mesh.js`, to use it you
put `var mesh = require('./javascript/mesh.js')` in your code, updating the
path if necessary.

The mest object gets built mainly in mesh-discovery.js and holds information
about known nodes. In mesh-discovery.js whenever a new connection is made
using the DHT network it is added to the mesh object.

Currently the structure used is:

mesh = {
  "connections": [
    serverID1: connectionInfo1,
    serverID2: connectionInfo2,
    ...
  ]
}

each connectionInfo is an object that holds information about that
connection.

{
  connection: connection,
  type: info.type,
  host: info.host,
  port: info.port,
  channel: info.channel.toString(),
  active: true,
  introductionNeeded: false
}

the connection part of that is the object used to actually send or receive
data from that server.

To send data to a server given its serverID you do this:

mesh.connections[serverID].write(message)

see mesh-discovery.js for examples of what messages look like.
