# Plugins

Dodo uses a plugin archetecture. The base Dodo code is a very simple setup that
just loads plugins. All the functionality of Dodo comes from plugins.

Plugins are sort of vaguely defined for now. At the moment all the plugins are
necessary for Dodo to funciton at all but in the future applications will be
built as plugins added to Dodo.

Plugins that are currently used:

- `mesh-discovery` this sets up the discovery swarm built on top of the
  bittorrent dht. This is what lets us discover and connect to remote servers
  without any central server.
- `database` this sets up the interface to the database we use by default. It
  is a simple sqlite database.
- `express-server` this sets up the express server used as the UI and, if the
  node is accessible on the open internet, an alternate way for the rest of the
  mesh to access the node.
- `data-handlers` this adds handler functions for the default message types
  between nodes.
- `server-requests` this adds functions for inter-server communication
