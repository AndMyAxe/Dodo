# Express Server

This is used for the UI and some interactions.

Pages are built using Set (https://ind.ie/labs/set/) templates are stored in
the `views` folder.

## Paths

Paths used by the server

### Unauthenticated GET paths

There are used to get publicly available information from the server.

- `/api/info` - returns server info if the server is set as public.
- `/api/public/servers/All` - returns info about all public servers that are
  known to the server in human readable format.
- `/api/public/servers/All/json` - returns info about all public servers that
  are known to the server in json format.
- `/api/public/people/All` - returns all public posts on the server in a human
  readable format.
- `/api/public/people/All/json` - returns all public posts on the server in
  json format.
- `/api/public/people/:name` - returns all public posts on the server from
  `:name` in human readable format. The GET is sent with a name which
  determines whose posts are returned.
  - `/api/public/people/:name/json` - returns all public posts on the server
    from `:name` in json format. The GET is sent with a name which determines
    whose posts are returned.

### Authenticated GET paths

There are no authenticated GET paths, it exposes data so we aren't using it
anywhere you would want anything authenticated.

### Unauthenticated POST paths

Paths that use `POST`

- `/api/authenticate` - to authenticate with the server.
  The body of the post needs to contain the name and password to use for
  authentication. You can see where this is used in `Pages/ui/index.html` in
  the `getToken` function.
- `/api/info` - get information about the server, if it is set as public.
- `/api/local/fetch` - get all public posts from the server. This is meant to
  be used by a remote server. The POST body should contain the `url` of the
  server where you want to fetch posts from.

### Authenticated POST paths

These paths only work if you have authenticated using `/api/authenticate`

- `/main` - displays the main interface for the sever (sending and checking
  posts, etc.)
- `/main/command` - Commands send to the server are POSTed to here.
- `/api/people/:name/post` - Posting data here lets you post as `:name` as long
  as you are authenticated as `:name`. The body of the post should contain:
  - `name` - the name of the person for the post
  - `text` - the post text
  - `public` - if the post is public or not
  - `conversation` - the conversanion (if any) that the post belongs to.
  - `postType` - the post type (currently only `toot`)
- `/api/people/All` - this returns all posts in a human readable way.
- `/api/people/All/json` - this returns all posts in json format.
- `/api/people/:name` - this returns all posts by `:name` in a human readable
  way.
- `/api/people/:name/json` - this returns all posts by `:name` in json format.
