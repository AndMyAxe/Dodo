# Data Handlers

To make extending Dodo easier handling incoming messages is done using data
handling functions. Messages sent between nodes are in JSON format. Each
message has at least one property, `t`, which defines the type of message.

This message is passed to a data handler function. The name of the function
matches the message type.

Every data handler function is in the global `handlers` object. To add new
message types you extend the data handlers by adding new functions to the
object. Each handler function gets 3 inputs:

- `data` - the data payload that came with the message, if any.
- `connection` - the connection object. To respond create the json object (or
  whatever type of response you need to make) and send it using
  `connection.write(message)`
- `info` - information about the connection that sent the message.

```
var handlers = require('./handlers.js')

handlers.newHandlerName = function (data, connection, info) {
  // check if the message type is correct
  if (data.t === 'newHandlerName') {
    // respond with a ping as a demo. All responses must be stringifyed
    var message = JSON.stringify({'t': 'ping', 'body': 'Oh look! A test ping'})
    // Send the stringifyed response to the same node that sent us a message.
    connection.write(message)
  }
}
```

Then in your Local.json configuration file add the file with your data handlers
to the `handler-paths` array. Use `Config.json` as an example to see how to do
it. The paths are relative to the `javascript` folder.

By the magic of require this will make your data handling function available
anywhere you require the `handlers.js` file.
