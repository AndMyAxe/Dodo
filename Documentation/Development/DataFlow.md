# Data Flow

This is notes about how the nodes interact with each other.

The plan is to have Dodo use a plugin archetecture so that it can easily be
extended.

For the most part the plugins would extend behaviour in the
`Actually sending data` section. I would like to make it possible to modify
most of the behaviour, even adding new types of nodes like the introduction
nodes.

## Connecting to the DHT

Open problems:

- How do we set up our own bootstrap servers? Right now I think that the
initial connection to the swarm is taken care of by the torrent network
servers.

This is mostly taken care of already, I would just like to be able to have the
option of using any bootstrap servers. I think that is mainly done by setting
up a server on a publicly available ip and listing it.

## Creating connections from behind NATs

This assumes that both Jed and Ekaitz have already connected to the DHT

Open problems:

- How does Jed know which introduction nodes Ekaitz uses?
- We haven't implemented the UDP hole punching yet but we know it works.

Jed and Ekaitz both know the other exists but don't necessarily know the others
current address or port information.

Jed and Ekaitz are both behind NATs, this information is included in the DHT so
it is known to both of them.

Since they are behind NATs each person has selected a number of introduction
nodes to use. They maintain their connection to the mesh by periodically
pinging their set of introduction nodes.

Jed wants to talk to Ekaitz.

If there has been contact between Jed and Ekaitz in the past than the first
attempt is to directly contact Ekaitz using a known address and port. If that
succeeds than we are done.

Assuming that the first attempt fails Jed sends a message to an introduction
node used by Ekaitz (HOW IS THIS NODE FOUND? The simplest way would be the
closest introduction node in XOR space) and requests an introduction.

As long as Ekaitz is online the node is maintaining a connection with Ekaitz.
The node then does the normal UDP hole punching process to allow Jed and Ekaitz
to talk to each other.

## Actually sending data

Open problems:

- Implementation of all of this
- Where in this process should we put authentication?
- We can have all packets sent signed, should we do that here? We won't always
have anything to check against but we can use signing to prevent tampering.

There is a live connection between Jed and Ekaitz, now they want to acually
use it for something.

Data is sent between nodes as serialized JSON objects.

The receiving node handles the data using the connection.on('data', ...)
function inside the code for the discovery swarm.

This function parses the incoming data stream. At that point it is UDP data
which has no defined structure and is treated as a raw hex buffer.

The data is parsed as JSON. This should always be a JSON object and one of its
top-level properties is always `messageType` (maybe change the name, it may
just be `t` for type or something, we want it to be short since it is with
every message.)

Depending on the message type the body of the message is sent to a handler
specifically for that type. The handler will either be the actual application
or it will send the data to the appropriate application.

## Behaviour of introduction nodes

Introduction nodes are going to be used for both UDP/TCP hole punching and as
optional gate keepers for privacy or security.

Any node can act as an introduction node, but the most useful ones are nodes
that have static ip addresses that are not behind any sort of NAT.

### Gate keeper type introduction nodes

These nodes are set up so that they can be used to filter interaction with
another node. If you want to have a private node on the mesh than instead of
announcing your connection to the mesh on the DHT you can connect to a set of
trusted nodes (people you know or something similar) and connections to you
can be filtered using that node.

This filtering could take a number of different forms:

- Whitelisting (Only authorized people can connect to you)
- Blacklisting (A list of people who are specifically blocked)
- Moderated (each person or message is checked by the owner of the node and
they decide to pass it on or not)

This can be set so that all traffic must go through the introduction node, or
just make the initial connection be moderated by the introduction node.

### Hole-punching type introduction nodes

Hole punching nodes are nodes that are accessible to nodes behind troublesome
NATs. For the most part this means nodes that have static ip addresses and are
accessible normally from the open web. There may be a way to relax the
requirements a bit because a node can introduce any nodes that it is currently
connected to even if they are all behind NATs.

Each node that is behind a NAT will have a connection to some set of these
nodes. These connections need to be maintained in order to remain part of the
network.

This means that each node is always going to be connected to some node that can
do introductions, so the network will always be connected. The problem is going
to be finding which introduction nodes are connected to the node you wish to
connect to.

Introductions may be multi-step processes.

- You ask the introduction nodes that you are connected to if any of them are
connected to the node you wish to speak to, if so than they are used for the
hole punching.
- If you are not connected to an introduction node that is also connected to
the node you want to talk to than you ask the introduction node to find a node
connected to your destination node, then the introduction node connected to you
introduces you to the node connected to your desired node and then that node
introduces you to your desired node.
