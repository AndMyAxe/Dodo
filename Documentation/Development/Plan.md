# Plan

See `Terms.md` for definitions.

## Interface

- A view that displays nodes you are currently connected to
- A view that displays the nodes that you have knowledge of (but aren't necessarily connected to)
- A button that tries to send a ping to any known node to try and create a connection
- A button that requests an introduction to a node
- A button that retrieves posts from a remote node

## Connections

-

## Database

- Create the database for the remote posts
