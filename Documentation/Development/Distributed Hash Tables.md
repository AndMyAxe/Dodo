# Distributed Hash Tables

In Dodo distributed hash tables (DHTs) are used as a way to share the location
(ip address and port) of nodes without a central server. This is required to
let nodes find each other and communicate without a central authority.

An explanation of the how the DHT works in a theoretical sense can be found
here http://www.cs.rice.edu/Conferences/IPTPS02/109.pdf

The DHT used is bittorrent-dht, which was originally created (and still
maintained) to allow trackerless torrents on the bittorrent network.

An explanation of how the bittorrent-dht works can be found here:
http://www.bittorrent.org/beps/bep_0005.html

The implementation we use is discovery-swarm, and by extension
discovery-channel, which uses the webtorrent version of the bittorrent-dht
discovery-channel also uses dns-discovery (I am not sure if that is every used
by what we do)
https://github.com/mafintosh/discovery-swarm
https://github.com/maxogden/discovery-channel
https://github.com/webtorrent/bittorrent-dht
https://github.com/mafintosh/dns-discovery

I don't think that the discovery swarm checks to make sure a key doesn't exist
before adding it again. So a node may have multiple entries in the table if it
has left and joined the network multiple times.

There are multiple things to keep track of:

- The swarm - this may be a global construct, any hashtable made using this may
  be part of the swarm (as long as it is accessible to the outside world)
