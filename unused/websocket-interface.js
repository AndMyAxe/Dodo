/*
    Websockets are one way to communicate between the browser interface and the
    node process.

    This sort of control should be limited to localhost because opening up this
    to the internet at large is probably a huge security risk.
*/

// This is our websocket server
const WebSocketServer = require('ws').Server
// Include the database things here so we can use websockets to control things
//const database = require('./database.js')
const wss = new WebSocketServer({noServer: true})
let connections = []

const start = function () {
  console.log('starting websocket-interface')
  wss.on('connection', handleConnection)
}

/*
  This is a placeholder for the future when we want to authenticate messages.
*/
function authenticateMessage(eventData) {
  return true
}

function handleMessage(event) {
  console.log(event)
  try {
    let eventData = JSON.parse(event);
    // Make sure we have a handler for the message type
    if(typeof messageHandlers[eventData.type] === 'function') {
      // Check authorisation
      const authorised = authenticateMessage(eventData)
      if(authorised) {
        eventData.decoded = authorised
        messageHandlers[eventData.type](eventData);
      }
    } else {
      console.log('No handler for message of type ', eventData.type);
    }
  } catch (e) {
    console.log('An error while trying to handle a message', e)
  }
}

function handleConnection (client) {
  console.log('new connection here')
  connections.push(client)
  client.on('message', handleMessage)
}

module.exports = wss
module.exports.start = start
