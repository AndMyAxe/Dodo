/*
    This file has all the pieces required to use localtunnel for testing dodo
*/

// this is to make everything accessible to the outside world
/*
    TODO
    Don't use local tunnel. This is a big task for later.
*/
var localtunnel = require('localtunnel')

var url = ''

var start = function () {
  // This sets up the local tunnel thing
  var tunnel = localtunnel(process.env.npm_package_config_port, function (err, tunnel) {
    if (err) {
      console.log(err)
    } else {
      url = tunnel.url
      console.log(tunnel.url)
    }
  })

  // This never actually gets called from what I can tell.
  tunnel.on('close', function () {
    console.log('tunnel closed')
  })
}

module.exports = localtunnel
module.exports.start = start
module.exports.url = url
