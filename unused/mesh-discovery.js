/*
  This file handles all of the mesh discovery and connection things.

  It uses discovery-swarm to connect to the DHT and handle connections and then
  forwards data sent over the connections to the appropriate handlers
*/

/*
  The way this is going to work is the discovery swarm gives a connection to
  the DHT, we can use that to look up the address and ports used by peers.

  After we have the peer information we can create a connection to them.
  If they are not behind a NAT than we can just send them a HELLO packet

  If they are behind a NAT than we have to use an introduction server to create
  a connection before sending the HELLO packet

  Packets are going to be JSON objects, they get stringified for transmission

*/

/*
  Each packet sent will have a type, each type will be handled by its own
  handler.

  A handler is a function that does something with the data it is given. If the
  packet is information about the server than the handler would be a function
  that adds that server to the database or updates the servers information in
  the database.
*/

// This loads the configuration from a json file
var config = require('../LoadConfig.js')
// This is the discovery swarm object
var swarm = require('discovery-swarm')
// This imports the handlers for received data
var dataHandlers = require('./handlers.js')

// Placeholder object, in the future this may hold information about the swarm
var mesh = require('./mesh.js')

// Crypto for making the UUID if needed.
var crypto = require('crypto')

/*
  This function connects to the swarm and to the channel defined in the config
  object and then sets up the handler functions for data received
*/
var joinSwarm = function () {
  // There shouldn't be a way for mesh.connections to exist, but in case it
  // does don't overwrite it here.
  mesh.connections = mesh.connections || [];
  // We want to encourage udp connections, but we may not need that here.
  // We use UDP because hole punching is less reliable with TCP than with UDP.
  var UUID_Thing = config.plugins.discoveryswarm.settings.id || false
  if (!UUID_Thing) {
    // If this node doesn't have a UUID yet than generate one.
    UUID_Thing = crypto.randomBytes(32).toString('hex')
    // Save the UUID to the local config
    var setting = {'plugins': {'discoveryswarm': {'settings': {'id': UUID_Thing}}}}
    config.saveSetting(setting)
  }
  var sw = swarm({tcp: false, id: UUID_Thing})
  // Set up port and channel for the discovery swarm to use.
  sw.listen(config.plugins.discoveryswarm.settings.port, function () {
    console.log(`Discovery swarm on port
      ${config.plugins.discoveryswarm.settings.port}`)
  })
  sw.join(config.plugins.discoveryswarm.settings.name, function () {
    console.log(`joined ${config.plugins.discoveryswarm.settings.name}`)
  })
  sw.on('connection', function (connection, info) {
    var serverID = info.id.toString('hex')
    console.log(`Connected to server with id: ${serverID}`)
    // Use the connection id in hex as the ID for the connection. We will add
    // We will add a human readable name to the object for the connection.
    if (!mesh.connections[serverID]) {
      console.log('a new connection')
      // If we don't have information about this server id add it to our known
      // connections object.
      mesh.connections[serverID] = {
        connection: connection,
        type: info.type,
        host: info.host,
        port: info.port,
        //channel: info.channel.toString(),
        active: true,
        introductionNeeded: false
      }
      // Send a message to the remote server asking for the server info to test
      // if we can connect to the node without an introduction.
      // If we get a response than we don't need an introduction, if we don't
      // get a response than we do.
      var message = {'t': 'info', 'body': `info request from ${config.server.name}`, 'name': config.server.name}
      connection.write(JSON.stringify(message))
      // TODO uppon reseiving the response information we need to save it in
      // the database.
    } else {
      console.log('a known connection')
      // We already have some info about the connection.
      // Set the connection as active.
      mesh.connections[serverID].active = true
      // Send a ping to let the remote server know the connection is
      // active
      var message = {'t': 'ping', 'body': `ping from ${config.server.name}!`}
      connection.write(JSON.stringify(message))
    }
    // TODO we need to build a list of connections that the node has that lists
    // information about the nodes and if we need introductions

    connection.on('data', function (data) {
      // TODO We need to write a handler to respond to pings correctly

      // TODO we need to write some handlers for other message types.
      //console.log(data.toString())
      try {
        var jsonOut = JSON.parse(data.toString())
        // Check to make sure that jsonOut.t exits and is a string
        if (typeof jsonOut.t === "string") {
          // Check to make sure that there is a handler for the packet type
          if (typeof dataHandlers[jsonOut.t] === "function") {
            // Pass the received data as a json object to the appropriate data
            // handler
            dataHandlers[jsonOut.t](jsonOut, connection, info)
          }
        }
      } catch (err) {
        console.log(err)
      }
    })
  })
  /*
  sw.on('peer', function (peer) {
    console.log('New peer!')
    console.log(peer)
    console.log(`Connecting peers: ${sw.connecting}`)
    console.log(`queued peers: ${sw.queued}`)
    console.log(`connected peers: ${sw.connected}`)
    //console.log(`Peer on channel ${peer.channel.toString()}`)
  })
  */
}

// Add a peer
// name is channel name
// peer is {host: host, port: port}
// sw.addPeer(peer, name)

module.exports = mesh
module.exports.start = joinSwarm
