/*
  This file holds the core data handler functions. More can be added in plugins
  to extend the functionality of Dodo.

  Each handler function is passed the parsed data as a json object and the
  connection that the data came from.

  The shape and contents of the data object are determined by the type.
  The only part that is required is that they have a t property that holds a
  string that lists the packet type:
  {
    't': 'sometype'
  }
  where 'sometype' is the packet type.
  It can have any number of other properties.
*/

// the dgram module is required so that we can make UDP connections as needed.
//var dgram = require('dgram')

// Get the global configuration
var config = require('../../LoadConfig')
// Get the global listing for connections on the mesh
var mesh = require('../mesh.js')
// Initialise the handlers object
var handlers = require('../handlers.js')
// The database interface so we can use that
//var database = require('../database.js')

/*
  This function responds to pings from other nodes.
  Pings are required because they maintain open UDP connections created by
  hole punching.

  TODO let the owner set paramaters about what pings will receive responses.
  You may want to refuse to respond to some pings if you don't want to connect
  to a node for whatever reason
*/
handlers.ping = function (data, connection, info) {
  if (data.t === 'ping') {
    connection.write(JSON.stringify({'t': 'pong', 'body': `pong from ${config.server.name}`}))
    console.log(data.body)
  }
}

/*
  This function handles when a response to a ping is heard.
  This confirms that there is a usable connection to the node that the ping was
  sent to.

  TODO figure out the best thing to do here. I think that we should have at
  least an option to set a timeout for sending a new ping later so that you
  have periodic pings to keep the connection alive
  TODO figure out how long the hole made by UDP hole punching lasts so we know
  how often we need to ping a node
*/
handlers.pong = function (data, connection, info) {
  if (data.t === 'pong') {
    console.log(data.body)
  }
}

/*
  This function handles a request for information about the server.
  It sends the human readable name, server type, if the server is public and if
  the server does introductions.
  TODO add to this!
*/
handlers.info = function (data, connection, info) {
  console.log('Sending Server Info')
  if (data.t === 'info') {
    var serverInfo = {
      'name': config.server.name,
      'serverType': config.server.serverType,
      'public': config.server.public,
      'introductionnode': config.server.introductionnode
    }
    connection.write(JSON.stringify({'t': 'infoResponse', 'body': serverInfo}))
  }
}

/*
  This function handles the response to an info request.
  TODO add to this!
*/
handlers.infoResponse = function (data, connection, info) {
  console.log('Received Server Info From', data.body.name)
  if (data.t === 'infoResponse') {
    mesh.connections[info.id.toString('hex')].name = data.body.name
    database.StoreServerInfo.run({'$serverName': data.body.name, '$serverURL': `${info.host}:${info.port}`, '$serverGroup': info.channel.toString(), '$serverType': data.body.serverType, '$serverAccessMethod': 'DHT', '$public': data.body.public, '$serverID': info.id.toString('hex')})
  }
}

/*
  This function handles when an introduction request is received

  Before anything else happens the node checks to see if it is configured to be
  an introduction node, if not than this request is ignored.

  LocalNode is the local node.
  NodeA is the node requesting the introduction
  NodeB is the node that NodeA wants an introduction to

  For this function LocalNode checks to see if it has an active connection to
  NodeB, if it does than it initiates UDP hole punching for NodeA and NodeB

  NodeA can identify nodeB either by host and port or by name.
*/
handlers.introRequest = function (data, connection, info) {
  if (data.t === 'introRequest') {
    var nodeBID = false;
    Object.keys(mesh.connections).forEach( function(id, index, array) {
      if ((mesh.connections[id].host === data.nodeInfo.address && mesh.connections[id].host === data.nodeInfo.port) || data.nodeInfo.name === mesh.connections[id].name) {
        nodeBID = id
      }
    })
    // Test to make sure we don't have a malformed packet.
    if (data.nodeInfo) {
      if (data.nodeInfo.address && data.nodeInfo.port) {
        // send an introduction packet to NodeA with the connection info for
        // NodeB and send an introduction packet to NodeB with the connection
        // info for NodeA
        // NodeA is the node that initiated the introduction. It gets a packet
        // that has the name info that NodeA sent to the LocalNode
        var packetA = JSON.stringify({'t': 'introduction', 'nodeInfo': data.nodeInfo})
        // NodeB gets a packet that has the information for NodeA
        var packetB = JSON.stringify({'t': 'introduction', 'nodeInfo': {'address': connection.address().address, 'port': connection.address().port}})

        // Send one packet to one node and the other to the other node.
        // Then each of those nodes sends a message to the other, this should
        // let us punch a hole.

        // packetB gets sent to the node who initiated the request, packetA
        // gets sent to the other node.

        // We are passed the connection to the initiating node, so just use
        // that to send a message to them.
        connection.write(packetB)

        // We have to find the connection to the other node in our list of
        // connected nodes.
        mesh.connections[id].connection.write(packetA)
      }
    }
  }
}

handlers.getConnectionList = function (data, connection, info) {
  if (data.t === 'getConnectionList') {
    //Get the list of connected people here
    var connectedPeople = Object.keys(mesh.connections).map(function(id){return mesh.connections[id].name})
    var message = JSON.stringify({
      type: 'connections',
      peers: connectedPeople
    })
    connection.write(message)
  }
}

/*
  This function handles when a node receives an introduction packet.

  For now there is no distinction made between if the LocalNode initiated the
  introduction or not because the behaviour is the same.

  Later we may want to change that so a node can be more private and reject any
  introductions that the LocanNode didn't initiate.

  TODO also we may want to make it so that a node will only act on
  introductions that come from a trusted node.
*/
handlers.introduction = function (data, connection, info) {
  // The LocalNode starts by sending a ping to the node it is being introduced
  // to

  // Make the packet as a json object
  var pingPacket = {'t': 'ping', 'body': 'ping'}
  // Convert the packet to a string
  var packetString = JSON.stringify(pingPacket)

  var socket = utp()
  socket.send(packetString, 0, packetString.length, data.nodeInfo.port, data.nodeInfo.address)

  // TODO figure out if this will make the nodes add the connection using the
  // normal discovery swarm thing or if we need to add it manually to the list.
  // Also, does this work?
}

/*
  This handles fetchAllPublicPosts requests.
*/
handlers.fetchAllPublicPosts = function (data, connection, info) {
  if (data.t === 'fetchAllPublicPosts') {
    database.GetAllPublicPosts.all({'$name': req.params.name}, function (err, rows) {
      if (err) {
        console.log(err)
      } else {
        console.log(JSON.stringify(rows))
      }
    })
  }
}

/*
  This function handles text packets.

  A text packet just contains a string and for the moment they are only used
  for testing.

  This function just prints out the text of the packet.
*/
handlers.text = function (data, connection, info) {
  if (data.text) {
    console.log(data.text)
  }
}

/*
  This function handles chat messages
*/
handlers.chatMessage = function (data, connection, info) {
  console.log('received data ', data)
  var chatServer = require('../chat-server.js')
  var newMessage = {
    text: data.message,
    name: data.from,
    time: data.time
  }
  handlers.updateHistory(newMessage)
  handlers.addMessage(newMessage)
}

module.exports = handlers
