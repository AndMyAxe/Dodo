/*
  This defines the different functions that Dodo uses  for communication
  between servers and storing or displaying the returned results.

  These are functions for outgoing communication initiated by the local node.
  Reactions to incoming data from a remote node are covered by data handlers.
*/

// This is if we are using HTTP or HTTPS responses
var request = require('request')

// This is so that we can extend the possible requests without modifying files
var serverRequest = require('../requests.js')

// Require the mesh object.
var mesh = require('../mesh.js')

// This loads the configuration from a json file
var config = require('../../LoadConfig.js')

/*
  responses that we care about here are going to be JSON.
  In the future other response types should be possible for plugins
  response structure
  {
    type: typeString,
    server: serverURL,
    options: optionsObject,
    body: bodyContents
  }

  type is the type of response, so far serverInfo and posts should be enough
  options is a json object that hold options (what options?)
  bodyContents is probably a json object
*/

/*
  This function will get all public posts from a server and save them to the
  local database so they can be displayed.
*/
serverRequest.FetchAllPublicPosts = function (data) {
  if (data.method !== 'server') {
    var message = JSON.stringify({'t': 'fetchAllPublicPosts'})
    mesh.connections[id].connection.write(message)
  } else {
    request(
      {
        method: 'GET',
        uri: server + '/api/public/people/All/json'
      },
      function (err, response, body) {
        if (err) {
          console.log(err)
        } else {
          console.log(server)
          console.log(response)
          console.log(body)
        }
      }
    )
  }
}

/*
  This function will get all the information about public servers from a server
  and save the results.

  This is for discovering more of the network and connecting to people.
*/
serverRequest.FetchAllPublicServers = function (data) {
  if (data.method !== 'server') {
  } else {
    request(
      {
        method: 'GET',
        uri: server.URL + '/api/public/people/All/json'
      },
      function (err, response, body) {
        if (err) {
          console.log(err)
        } else {
          handleServerResponse(response, body)
          // console.log(response)
          // console.log(body)
        }
      }
    )
  }
}

/*
  If you have the URL of a server you use this to get and save the information
  needed to add a connection to that server.

  The input argument must be an object that has at least a url property that
  lists the url of the server.

  Example: {url: localhost:3000}
*/
serverRequest.getServerInfo = function (data) {
  // For now default to using the dht connections
  if (data.method !== 'server') {
    var message = JSON.stringify({'t': 'info'})
    /*
    var id = Object.keys(mesh.connections)[0];
    if (id) {
      mesh.connections[id].connection.write(message)
    }
    */
    Object.keys(mesh.connections).forEach(function(id) {
      mesh.connections[id].connection.write(message)
    })
  } else {
    request(
      {
        method: 'GET',
        uri: 'http://' + data.url + '/api/public/people/All/json'
      },
      function (err, response, body) {
        if (err) {
          console.log(err)
        } else {
          // the body has the post contents
          console.log(body)
        }
      }
    )
  }
}

serverRequest.getConnectionList = function (data) {
  if (data.method !== 'server') {
    var message = JSON.stringify({'t': 'getConnectionList'})
    mesh.connections[data.node].write(message)
  } else {
    // TODO this
  }
}

serverRequest.requestIntro = function (data) {
  if (data.method !== 'server') {
    // data.targetNode is the node you want to be introduced to. It should
    // either be the nodeID or the string name for it.
    var message = JSON.stringify({'t': 'introRequest', 'target': data.targetNode})
    mesh.connections[data.node].write(message)
  } else {
    // TODO Add this!
  }
}

/*
  This is a generic function that you use to get things from remote servers.
  What you get is determined by the options parameter. An empty or missing options parameter will result in getting the server info.

  Inputs:
  server - an object that has information about the server. At a minimum it
    must have the url used to access the server.
  options - an optional object that holds the required information for making
    the request. An invalid or missing options object should result in fetching
    the server info.

    The server parameter must exist and have at least a valid URL for accessing
    the server. It may optionally have many other things.
    url - the server url
    endpoints - an object that lists the available endpoints for the server
    tokenInfo - an object that holds the access token for the server, if any as
      well as any related information like the expiration
    TODO figure out what else goes here

    The options parameter can have the following options (maybe more later):
    type - the request type. (info for server info, public for public posts, etc.) TODO list the planned options!!
    token - the access token, if any.
    endpoint - the api endpoint to use if it isn't standard
    verb - the http verb to use (POST or GET) this may be modified later and
      should only be necessary for custom endpoints
    TODO figure out what else goes here
    TODO add checking to ensure we have a valid url and any other input
      validation we may want
*/
serverRequest.FetchFromServer = function (server, options) {
  /*
  if (server.url) {
    var request = new XMLHttpRequest()
    var verb = 'GET'
    var endpoint = '/api/info'
    var token = false
    if (options) {
      if (options.type) {
        endpoint = options.type.location
      }
      if (options.token) {
        // The token should only be sent when using POST
        // so we should have a check here.
        token = options.token
      }
      if (options.endpoint) {
        endpoint = options.endpoint.location
        if (options.endpoint.verb) {
          // the verb should be known unless it is a custom endpoint
          verb = options.endpoint.verb
        }
      }
    } else {
      options = null
    }
    // If no options argument is given assume a GET to /api/info at the url.
    request.open(verb, server.url + endpoint, true)
    request.onreadystatechange = function () {
      if (request.readyState === 4 && request.status === 200) {
        // Check if the request was successful
        if (request.responseText) {
          var response = JSON.parse(request.responseText)
          if (response.success) {
            //Figure out how to handle the response
            responseHandlers[response.handler](server, options, response);
            //handleServerResponse(server, options, response);
          } else {
            handleServerRejection(server, options, response)
          }
        }
      }
    }
    request.send()
  } else {
    console.log('No server url given!!')
  }
  */
}

module.exports = serverRequest
