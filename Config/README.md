# Configuring Dodo

First, if you don't have a `Local.toml` already and want to use something other
than the default configuration, rename `Example-Local.toml` to `Local.toml`

If you are just testing Dodo than you probably want to start with the default
settings because that will let you join the rest of the network but the other
nodes will know that your node is just for testing.

Unless you are doing development on the Dodo core itself you should never need
to edit `Config.toml`.
If you want to change the settings than put the value you want in `Local.toml`
and it will be used instead.
Changing or removing properties in `Config.toml` can make everything break,
even if you are changing settings on a part you aren't using because Dodo
determines what to used based on `Config.toml` and `Local.toml` and if a
property is missing it can cause a crash.

Make sure that you use proper json formating when editing `Local.toml` because
the parser is very strict and an extra or missing comma will prevent the parser
from working.

These are the current options available:

- `server` options - these are the options for the node itself, like the node
name.
  - `name`: This is the name of your node. It doesn't have to be unique, but if
    it is unique that makes everything less confusing.
  - `type`: This is the type of node you are running. Options include (not all
    of this is implemented yet):
    - `mesh-node` is the basic
      node type,
    - `test` is a node for testing, setting this value lets other nodes
      know to not expect your node to be persistent and that it may not work
      as expected.
    - `forwarding` is a node that is there just to act as a forwarder for other
      nodes. This can be useful for anonymous interactions and for when NAT
      transversal is causing problems.
  - `public`: A boolean value that sets if the node is public or not.
  - `port`: The port used to serve the local application (Default value: 3000)
  - `accesmethod`: This describes how to reach the node. At the moment
    everything should be `dht`, but if your node has a static ip address or
    other way to access it we will update this in the future.
  - `introductionnode`: This is a boolean value that sets if this node can be used to introduce other nodes. Introductions may be needed for some types
      of NAT transversal as well as possibly being used for access to closed
      groups.
- `express`: These are options for the express server used to server the local
  UI. At the moment this is used only by the node owner.
  - `port`: The local port for the express server.
  - `localonly`: A boolean value that lets you set if the express server is
    only accessible on localhost (that is on the machine running the node) or if
    it should be accessible from anywhere on the local network. If you have an
    outward facing static IP this may make your server available from anywhere
    on the internet, be careful.
- `chat`: settings for the basic chat app built on dodo
  - `port`: default port for the chat app
