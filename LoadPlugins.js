/*
  This file loads all the plugins that are listed in the configuration files.
*/

// Get the configuration
let config = require('./LoadConfig.js');
// Get the handlers object
let handlers = require('./javascript/handlers.js')
// Get the requests object
let requests = require('./javascript/requests.js')
// Get the mesh object
let mesh = require('./javascript/mesh.js')

// Initialise an empty object to hold the plugin objects.
let plugins = {}

const addPlugins = function (config) {
  // If there are any plugins listed require each one.
  // We don't pass anything to the plugins it initialise them because inside each
  // plugin we require the config object which contains any needed paramaters.
  Object.keys(config.plugins).forEach(function (plugin, index, array) {
    // Only load plugins if they are set as active.
    if (config.plugins[plugin].active === true) {
      // Each plugin listing has a sub-property path that contians the path to
      // the plugin for the require.
      plugins[plugin] = require(config.plugins[plugin].path)
      console.log('Add plugin: ',plugin)
      // If this plugin has a setup function than call it.
      if (typeof plugins[plugin].setup === 'function') {
        plugins[plugin].setup(config, handlers, requests, mesh)
      }
      // If this plugin has a start function than call it.
      if (typeof plugins[plugin].start === 'function') {
        plugins[plugin].start(config, handlers, requests, mesh)
      }
      // If this plugin has requests to add to the global requests object, add
      // them.
      if (typeof plugins[plugin].requests === 'object') {
        requests.addRequests(plugins[plugin].requests)
      }
      // If this plugin has data handlers to dad to the global object, add them
      if (typeof plugins[plugin].handlers === 'object') {
        handlers.addHandlers(plugins[plugin].handlers)
      }
    }
  })
}

// Add all the included plugins in the plugins object.
//addPlugins(config)

module.exports = plugins
module.exports.addPlugins = addPlugins
