/*
  This is the start of a very simple distributed network test in Nodejs

  to start:

  npm start

  to start with localtunnel

  npm start --localtunnel

  There are different parts here:

  Reading your own database to view posts you have already
  Reading someone elses database to fetch posts from them to read or save
  Sending posts to another database

  It may be best to have a separate application for local administration.
  That would have local access to the database and you could do anything you
  want to your local database through it.
  Since messages are going to be signed you could modify the payload but it
  would be obvious to anyone else that the message has been modifed.

  So any front-ends should have some way to verify the cerdentials on a
  message and either not display or clearly label any that are suspected of
  being modified.

  Remember that the one person who can ever modify the database is the owner.
  You post to your own database and other servers can request from your
  database, there is never a time when a person other than the owner can
  write to a database. Getting posts from other people is something the owner
  does and then optionally saves those posts in the local database.

  BIG THING TO REMEMBER:

  Access to the database is either public (anyone who requests access can
  have it, no authentication), or private and from a known node.

  This means that interactions between nodes can be done using signed tokens
  instead of some sort of username and password set up. The only password
  that is ever needed is for the owner to access the database from a remote
  machine.
  That may not always be true but for a lot of things it is. So for unix
  systems unlocking the default keyring should be all that is needed for
  local access.

  This doesn't solve the problem of the initial public key exchange, but if
  the owner knows the location of a server that they want to connect to than
  they can request the public key of that server or possibly request the key
  from another node.
  We may go with an introduction model where you get a third party to
  introduce your node to another node to ease the distribution of keys and to
  add some layer of mediation or trust to the initial exchange.

  TODO
  Figure out a good way to set rules about what gets saved locally

  TODO
  Make the interactions between databases work out. We can use the existig
  api things since they return json data but we need to make whateven is
  going to make the call and then save the results.

  TODO
  Server locations may change, so we need to support this. People may not have
  static ips or may move some other way. So we need to be able to check if a
  server is online at the location we have saved and if not we need a way to
  check to see if the server has moved.
  For now this may require us to have directory servers. I would like any
  node to be able to act as one where it just passes on its most current
  information about server urls and locations.
  Lieing is doing to be difficult because everything should be signed and
  after the initial exchange of public keys between servers it should be
  almost impossible to lie about being a server.

  TODO
  Multiple devices connected to the same account

  I can think of two ways to do this, one is having a way for a person to log
  into their server from another device, but this requires a persistent
  server. Since a hope is that you can run this on a phone or your home
  computer this isn't going to work well if you don't have that on all the
  time.

  Another way to do this is to have a way for one account to say that they
  are the same as another account. Then both accounts have to be checked
  simultaneously and they have to agree. Then the two public keys can be
  associated with the same account when handling privacy and access.

  We need to be careful with this part. I am not sure how it would be abused
  to gain access but it may be possible.

  It may be easiest to set up access groups and always use them. Then we
  could model all access, even for private messages, as an access group.
  Private messages would just be shared with a group of 1 person.

*/

/*
  Require the needed modules
*/
// This loads the configuration from a json file
const config = require('./LoadConfig.js')
// This loads the plugins that are listed in the config object.
require('./LoadPlugins.js').addPlugins(config)
