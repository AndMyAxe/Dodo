# Dodo Structure

## Terms Used

Dodo has a few core concepts that are necessary to know in order to understand
Dodo and its code structure.

- `nodes` refer to any device running Dodo. Nodes are connected together using
  different types of links based on the nodes capabilities. Currently only
  direct websocket links are supported, but in the future more traditional
  http(s) apis will be added.
- The `Dodo Core` is the basic set of scripts that are necessary for the very
  minimal program. They just load plugins and provide a simple method for the
  plugins to communicate with each other.
- Communication is all done through `requests` and `handlers`
  - `requests` are functions used to initiate outgoing communications. This
  covers both communication between different plugins and communication with
  other Dodo nodes. The send outgoing data.
  - `handlers` are functions used to do the appropriate action in response
  to a request. They handle incoming data.
- `Plugins` do everything else. Plugins include the `hyperswarm` interface used
  to find other Dodo nodes, the `express-server` which sets up the default
  browser interface to Dodo and the `databases` plugin which gives an interface
  to the different methods of storing data.
- The `mesh` is the network connections. It is passed around as a json object
  that contains information about each known node on the network.

## Dodo Core

Dodo is made up almost completely of plugins, but there are some core
components needed to load and use the plugins.

The core (non-plugin) components of Dodo are:

- A script to load the configuration
- A script that, given the configuration, loads any plugins listed in the
  configuration.
- A script that creates a data structure to hold message handlers added by
  plugins
- A script that creates a data structure to hold request handlers added by
  plugins

Everything else is added in the from of plugins.

## Plugins

Plugins handle all of the functionality of Dodo.
There are a few plugins included with Dodo by default to give the basic
functionality, but any of them can be removed or replaced depending on what is
needed or desired by whoever is using Dodo.
Any new functionality will be added to Dodo using plugins.

Plugins are separated and are only allowed to communicate using `requests` and
`handlers`.
One plugin should never require another plugin (in the sense of foo =
  require('bar'), plugins can depend on other plugins).
When each plugin is initialised it is given:

- The configuration
- The data structure holding all of the `requests` functions
- The data structure holding all of the `handlers` functions
- The `mesh` object that holds information about all of the currently avaliable
  connections.

and each plugin can:

- Add to the configuration
- Provide `requests` to be added to the requests data structure
- Provide `handlers` to be added to the handlers data structure
- Run a `setup` function
- Run a `start` function to start the plugins active components, if any.

''Note:'' if a plugin has a request or handler that has the same name as an
existing request or handler than it will overwrite the existing one. So
whichever one is added last wins.

## Handlers and Requests

In general `handlers` are for incoming data and `requests` are for outgoing
data.
For inter-plugin communication each plugin has direct access to all of the
handler functions so they can call them directly.

''Note:'' In the future I may make it so that requests are added to a queue
and a single function handles determining if they will be processed or not, but
that is a future thing.

So for inter-plugin communication no `requests` need to be written.

For any communication with other nodes `requests` are required.

For every request that is sent to another Dodo node a corresponding handler
needs to be available on the other node or it will have no way to process the
request.

# Missing Things

I am not sure that this structure gives a reasonable way to setup front-end
plugins.
We can have a consistent communication between the server component and the
front-end, but I am not sure that the current structure gives a way to change
which front-end is used unless you change the entire express-server component.
